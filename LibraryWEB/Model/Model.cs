﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;

namespace LibraryWEB.Model
{
    public class Book
    {
       
            public int? ID { get; set; }  //nullable

            public string Author { get; set; }
            public string Title { get; set; }
            public string Year { get; set; }

            public Book()
            {
            Debug.WriteLine("Book created");
            }
            public Book(string author, string title, string year)
            {
                this.Author = author;
                this.Title = title;
                this.Year = year;
            }

            public Book(int ID, string author, string title, string year)
            {
                this.ID = ID;
                this.Author = author;
                this.Title = title;
                this.Year = year;
            }
      

    }
}
