using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using LibraryWEB.Model;

namespace ReactApp.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
       
        List<Book> books = new List<Book>();
        //...................................................READING.......................................................
        [HttpGet("[action]")]

        public IEnumerable<Book> readFile()
        {
            StreamReader file = new StreamReader(@"D:\Documents\education process\EGUI\LabsMine\Lab3\LibraryWEB\library.txt");
            string currentline;
            List<string> myList = new List<string>();

            //string[] words;

            int id = 1;
            while ((currentline = file.ReadLine()) != null)
            {
                string[] words = new string[3];
                words = currentline.Split('|');
                Book book = new Book(id, words[0], words[1], words[2]);
                books.Add(book);
                id++;

            }


            Debug.WriteLine(books);



            file.Close();

            return books;

        }

        //...................................................OVERWRITING.......................................................
        [HttpPut("[action]")]
        public bool overWriteFile([FromBody] Book[] booksFromReact)
        {

            FileInfo fi = new FileInfo(@"D:\Documents\education process\EGUI\LabsMine\Lab3\LibraryWEB\library.txt");
            using (TextWriter txtWriter = new StreamWriter(fi.Open(FileMode.Truncate)))
            {
                foreach (Book book in booksFromReact)
                {
                    string LineToWrite = book.Author + "|" + book.Title + "|" + book.Year + "\n";
                    txtWriter.Write(LineToWrite);

                }

                txtWriter.Close();
            }

            return true;
        }

        //........................................APPENDING..................................................
        [HttpPost("[action]")]
        public bool appendToFile([FromBody] Book bookFromReact)
        {

            string LineToWrite = bookFromReact.Author + "|" + bookFromReact.Title + "|" + bookFromReact.Year;
            System.IO.File.AppendAllText(@"D:\Documents\education process\EGUI\LabsMine\Lab3\LibraryWEB\library.txt", LineToWrite + Environment.NewLine);
            return true;
        }


        //.........................................FILTERING......................................................
        List<Book> FilterList = new List<Book>();

        [HttpPost("[action]")]
        public IEnumerable<Book> myFilter([FromBody]JObject FromReact)
        {
            FilterList.Clear();

            var ContentList = FromReact["bookscontent"].ToObject<List<Book>>();
            var BookToFilter = FromReact["booktobefiltered"].ToObject<Book>();


            foreach (Book book in ContentList)
            {
                Debug.WriteLine(book.Author);
                Debug.WriteLine(book.Title);
                Debug.WriteLine(book.Year);

            }

            Debug.WriteLine(BookToFilter.Author);
            Debug.WriteLine(BookToFilter.Title);
            Debug.WriteLine(BookToFilter.Year);

            //string to be filtered from FIlter inputs
            int? IDToBeFiltered = BookToFilter.ID;
            string AuthorToBeFiltered = BookToFilter.Author;
            string TitleToBeFiltered = BookToFilter.Title;
            string YearToBeFiltererd = BookToFilter.Year;

            // string YearToBeFiltered = YearsPool.Text;



            foreach (Book book in ContentList)
            {
                if (book.ID == IDToBeFiltered && book.Author == AuthorToBeFiltered && book.Title == TitleToBeFiltered && book.Year == YearToBeFiltererd)
                {
                    Debug.WriteLine("Absolute Matching");
                    FilterList.Add(book);
                    break;

                }
                //one of parameters matches exactly
                else if (book.ID == IDToBeFiltered)

                {
                    Debug.WriteLine(" One of the parameters matches Exactly");
                    FilterList.Add(book);
                    continue;
                }
                

                else if (book.Author.Length != 0 && book.Author == AuthorToBeFiltered)

                {
                    Debug.WriteLine(" One of the parameters matches Exactly");
                    FilterList.Add(book);
                    continue;
                }
                else if (book.Title.Length != 0 && book.Title == TitleToBeFiltered)
                {
                    Debug.WriteLine(" One of the parameters matches Exactly");
                    FilterList.Add(book);
                    continue;
                }
                else if (book.Year.Length != 0 && book.Year == YearToBeFiltererd)
                {
                    Debug.WriteLine(" One of the parameters matches Exactly");
                    FilterList.Add(book);
                    continue;
                }


                //if a string we are trying to filter contains name of some book from the library
                else if (book.Author.Length != 0 && AuthorToBeFiltered.Contains(book.Author))

                {
                    Debug.WriteLine("Some item in library has parameter a part of a name");
                    FilterList.Add(book);
                    continue;
                }
                else if (book.Title.Length != 0 && TitleToBeFiltered.Contains(book.Title))
                {
                    Debug.WriteLine("Some item in library has parameter a part of a name");
                    FilterList.Add(book);
                    continue;
                }

            }

            foreach (Book book in ContentList)
            {
                if (book.Author.Contains(AuthorToBeFiltered) && AuthorToBeFiltered.Length != 0 && !FilterList.Contains(book))
                {
                    Debug.WriteLine(AuthorToBeFiltered);
                    Debug.WriteLine("Parameters name contains some item from library");
                    FilterList.Add(book);

                }
                else if (book.Title.Contains(TitleToBeFiltered) && TitleToBeFiltered.Length != 0 && !FilterList.Contains(book))
                {
                    Debug.WriteLine(AuthorToBeFiltered);
                    Debug.WriteLine("Parameters name contains some item from library");
                    FilterList.Add(book);

                }

            }


            foreach (Book book in FilterList)
            {
                Debug.WriteLine(book.Author);
                Debug.WriteLine(book.Title);
                Debug.WriteLine(book.Year);

            }

            return FilterList;


        }



    }



}


