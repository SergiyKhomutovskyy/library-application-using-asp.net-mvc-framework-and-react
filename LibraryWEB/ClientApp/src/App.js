import React, { Component } from 'react';
import { Route } from 'react-router';
import { MainWindow } from './components/MainWindow';


export default class App extends Component {
  static displayName = App.name;

  render () {
      return (
        <div>
              <Route exact path='/' component={MainWindow} />
         </div>
    );
  }
}
