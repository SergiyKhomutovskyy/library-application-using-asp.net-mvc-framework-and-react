﻿import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col, Container } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';

export default class AddWindow extends Component {
    constructor(props) {
        super(props);
        this.state = {

            Author: "",
            Title: "",
            Year: "",

            showModal: props.showModal,
        }
    }

    openModal = () => {
        this.setState({
            showModal: !this.state.showModal
        })
    }

    AuthorChangeHandler = event => {
        this.setState({
            Author: event.target.value,

        });
        console.log(this.state.Author);
    }

    TitleChangeHandler = event => {
        this.setState({
            Title: event.target.value,

        });
        console.log(this.state.Title);
    }

    YearChangeHandler = event => {
        this.setState({
            Year: event.target.value,

        });
        console.log(this.state.Year);
    }

    CancelButton = () => {

        this.setState({
            Author: "",
            Title: "",
            Year: "",
        });

        this.props.handleClose();

    }

    SaveAndClose = () => {
        console.log("SAVEANDCLOSE WAS CALLED");

        let Book = {};

        Book.author = this.state.Author;
        Book.title = this.state.Title;
        Book.year = this.state.Year;

        this.props.PassData(Book);


        this.setState({
            Author: "",
            Title: "",
            Year: "",
        });

        this.props.handleClose();

    }

    InputsEmpty() {

        if (this.state.Author.trim() == "" && this.state.Title.trim() == "" && this.state.Year.trim() == "")
            return true;
        else
            return false;
    }

    render() {
        return (
            <Modal isOpen={this.props.showModal} className={this.props.className}>
                <ModalHeader> Modal title</ModalHeader>
                <ModalBody>

                    <Form>
                        <FormGroup>
                            <Label for="exampleAuthor">Author</Label>
                            <Input type="Author" name="email" id="exampleEmail" placeholder="please provide Author" onChange={this.AuthorChangeHandler} />
                        </FormGroup>

                        <FormGroup>
                            <Label for="exampleTitle">Title</Label>
                            <Input type="Title" name="password" id="examplePassword" placeholder="please provide Title" onChange={this.TitleChangeHandler} />
                        </FormGroup>

                        <FormGroup>
                            <Label for="exampleYear">Year</Label>
                            <Input type="Year" name="password" id="examplePassword" placeholder="please provide Year" onChange={this.YearChangeHandler} />
                        </FormGroup>


                    </Form>

                </ModalBody>

                <ModalFooter>
                    <Button color="primary" disabled={this.InputsEmpty()} onClick={() => this.SaveAndClose()} >Save </Button>

                    <Button color="secondary" onClick={() => this.CancelButton()} >Cancel</Button>
                </ModalFooter>
            </Modal>
        );
    }
}
