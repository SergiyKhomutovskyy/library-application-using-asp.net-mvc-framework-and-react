﻿import React, { Component } from 'react';

import { Form, FormGroup, Label, Input } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';
//import { Modal } from 'reactstrap';
import { Button } from 'reactstrap';
//import { Table } from 'reactstrap';

import AddBook from "./AddWindow/AddWindow.js";
import EditBook from "./EditWindow/EditWindow.js";

//import { render } from "react-dom";
//import matchSorter from 'match-sorter'

import ReactTable from "react-table";
import "react-table/react-table.css";



 export class MainWindow extends Component {
    static displayName = MainWindow.name;

    constructor() {
        super();
        this.state = {
            lastid: {},

            showModalAdd: false,
            showModalEdit: false,

            selected: null,
            rownumber: null,

            currentbook: {},

            libraryChanged: false,

            books: [],
            filteredbooks: [],
            FilterMode: false,

            FilterIDInput:"",
            FilterAuthorInput: "",
            FilterTitleInput: "",
            FilterYearInput: "",

            IDFilterInputText: "",
            AuthorFilterInputText: "",
            TitleFilterInputText: "",
            YearFilterInputText: "",


        }

        fetch('api/SampleData/readFile')
            .then(response => response.json())
            .then(data => {

                this.setState({ books: data, loading: false, });
                var LastBook = this.state.books.slice(-1)[0];
                //var LastBookId: {};

                if (this.state.books.length == 0) {
                    this.setState({ lastid: 0, });
                }
                

                console.log(this.state.lastid);
                console.log(this.state.books);
                console.log(this.state.selected);
            });


    }

    //.......................ADD/EDIT OPEN/CLOSE ROUTINE..................................
    openModalAdd = () => {
        this.setState({ showModalAdd: !this.state.showModalAdd });
        console.log("Open: " + this.state.showModalAdd);
    }

    closeModalAdd = () => {
        console.log("closed.")
        this.setState({ showModalAdd: false });
    }

    openModalEdit = () => {
        this.setState({ showModalEdit: !this.state.showModalEdit });
        console.log("Open: " + this.state.showModalEdit);
    }

    closeModalEdit = () => {
        console.log("closed.")
        this.setState({ showModalEdit: false });
    }

    //.............................DELETING.....................................

    DeleteSelectedBook = () => {

        console.log("IN DELETE FUNCTION!");
        var MainArray = this.state.books; // make a separate copy of the array
        var FilteredArray = this.state.filteredbooks;

        var index = this.state.selected
        console.log(this.state.currentbook.id);

        if (this.state.FilterMode == true) {
            //firstly in filtered view
            this.setState({
                filteredbooks: FilteredArray.filter(item => item.id !== this.state.currentbook.id),
                books: MainArray.filter(item => item.id !== this.state.currentbook.id),
            });
            //MainArray.filter(item => item.id !== this.state.currentbook.id);

        }
        else {
            if (index > -1) {
                this.state.books.splice(index, 1);
                this.setState({ books: MainArray }); //change the state of the urray to updated one
            }

        }

    }

    //...........................ADDING NEW BOOK.............................
    PassBook = (Book) => {

        console.log(Book);
        Book.id = this.state.books.length + 1;

        //this.setState({ lastid: Book.id });
        this.state.books.push(Book);

        fetch('api/SampleData/appendToFile', {
            method: 'POST',
            body: JSON.stringify(Book),
            headers: {
                'Content-Type': 'application/json'
            }

         

        }).then(res => console.log(res));

    }
    //.........................EDITING BOOK................................
    PassBookToEdit = (Book) => {

        console.log(Book);

        var MainArray = this.state.books; // make a separate copy of the array
        var FilteredArray = this.state.filteredbooks;
        var index = this.state.selected

        if (this.state.FilterMode == true) {

            FilteredArray.map(item => {
                if (item.id === this.state.currentbook.id) {
                    //item.id = Book.id;
                    item.author = Book.author;
                    item.title = Book.title;
                    item.year = Book.year;
                }
            });

            MainArray.map(item => {
                if (item.id === this.state.currentbook.id) {
                    //item.id = Book.id;
                    item.author = Book.author;
                    item.title = Book.title;
                    item.year = Book.year;
                }
            });

            this.setState({
                filteredbooks: FilteredArray,
                books: MainArray,
            });

        }
        else {
            if (index > -1) {

                const newBooks = this.state.books.slice() //copy the array
                newBooks[index] = Book //execute the manipulations
                this.setState({ books: newBooks }) //set the new state
            }

        }

        this.setState({
            currentbook:Book,
        });
    }



    //..............................FILTERING........................

    FilterButtonClicked() {

        let BookToBeFiltered = {};

        // console.log(this.state.FilterAuthorInput);
        //console.log(this.state.FilterTitleInput);
        //console.log(this.state.FilterYearInput);


        BookToBeFiltered.id = this.state.FilterIDInput;
        BookToBeFiltered.author = this.state.FilterAuthorInput;
        BookToBeFiltered.title = this.state.FilterTitleInput;
        BookToBeFiltered.year = this.state.FilterYearInput;

        console.log("FILTER BUTTON CLICKED");

        var value = {
            bookscontent: this.state.books,
            booktobefiltered: BookToBeFiltered,
        };

        fetch('api/SampleData/myFilter', {
            method: 'POST',
            body: JSON.stringify(value),
            headers: {
                'Content-Type': 'application/json'
            }

        }).then(response => response.json()).then(data => {

            console.log(data);

            this.setState({ filteredbooks: data, loading: false, FilterMode: true, });

            console.log(this.state.filteredbooks);
        });

        this.setState({
            FilterIDInput: "",
            FilterAuthorInput: "",
            FilterTitleInput: "",
            FilterYearInput: "",

        });

    }

     ClearButtonClicked() {

       

         this.setState({

             IDFilterInputText: "",
             AuthorFilterInputText: "",
             TitleFilterInputText: "",
             YearFilterInputText: "",

            FilterMode: false,

           /* IDAuthorInput: "",
            FilterAuthorInput: "",
            FilterTitleInput: "",
            FilterYearInput: "",*/
            
        });

    }

     IDFilterChangeHandler = event => {
         this.setState({
             IDFilterInputText: event.target.value,
             FilterIDInput: event.target.value,

             // ButtonDisabled : false
         });



         console.log(this.state.Author);
     }

    AuthorFilterChangeHandler = event => {
        this.setState({
            AuthorFilterInputText: event.target.value,
            FilterAuthorInput: event.target.value,

            // ButtonDisabled : false
        });



        console.log(this.state.Author);
    }

    TitleFilterChangeHandler = event => {
        this.setState({
            TitleFilterInputText: event.target.value,
            FilterTitleInput: event.target.value,
            // ButtonDisabled: false
        });

        console.log(this.state.Title);
    }

    YearFilterChangeHandler = event => {
        this.setState({
            YearFilterInputText: event.target.value,
            FilterYearInput: event.target.value,
            //  ButtonDisabled: false
        });
    }



    //............................ON CLOSING THE WINDOW...................
    componentDidMount() {
        // Activate the event listener
        this.setupBeforeUnloadListener();
    }

    SaveDataBeforeExit = () => {

        fetch('api/SampleData/overWriteFile', {
            method: 'PUT',
            body: JSON.stringify(this.state.books),
            headers: {
                'Content-Type': 'application/json'
            }

        }).then(res => console.log(res));

    }

    // Setup the `beforeunload` event listener
    setupBeforeUnloadListener = () => {
        window.addEventListener("beforeunload", (ev) => {
            ev.preventDefault();
            return this.SaveDataBeforeExit();
        });
     };

     //......................VISIBILITY ISSUES................................
     FilterButtonVisibility() {

         if (this.state.FilterIDInput.trim() == "" && this.state.FilterAuthorInput.trim() == "" && this.state.FilterTitleInput.trim() == "" && this.state.FilterYearInput.trim() == "")
             return true;
         else
             return false;

     }

     ClearButtonVisibility() {

         if (this.state.FilterMode == true)
             return false;
         else
             return true;
     }


     DeleteEditButtonsVisibility() {
         if (this.state.selected == null) {
             return true;
         }

         else { return false; }

     }

     currentMode() {

         if (this.state.FilterMode == false) {
             return this.state.books;
         }
         else {
             return this.state.filteredbooks;
         }

     }

    
         
    
   
    render() {
        return (
            <div>
                <div className="container-fluid">
                    <h1>Filter Optons</h1>
                    <Form className="row"  >
                        <FormGroup className="col-sm" >
                            <Label for="FilterID"> ID </Label>
                            <Input ref="someName" type="text" style={{ width: "100%" }} value={this.state.IDFilterInputText} name="id" id="FilterInputText" placeholder="please provide ID" onChange={this.IDFilterChangeHandler} />
                        </FormGroup>
                        <FormGroup className="col-sm">
                            <Label for="FilterAuthor"> Author </Label>
                            <Input type="author" value={this.state.AuthorFilterInputText} style={{ width: "100%" }} name="author" id="author" placeholder="please provide Author" onChange={this.AuthorFilterChangeHandler} />
                        </FormGroup>
                        <FormGroup className="col-sm">
                            <Label for="FilterTitle">Title</Label>
                            <Input type="title" value={this.state.TitleFilterInputText} style={{ width: "100%" }} name="title" id="title" placeholder="please provide Title" onChange={this.TitleFilterChangeHandler} />
                        </FormGroup>
                        <FormGroup className="col-sm">
                            <Label for="examplePassword">Year</Label>
                            <Input type="year" style={{ width: "100%" }} value={this.state.YearFilterInputText} name="year" id="year" placeholder="please provide Year" onChange={this.YearFilterChangeHandler} />
                        </FormGroup>
                        <div className="col-sm" style={{ marginTop: "30px" }}>
                            <Button type="button" color="primary" onClick={() => this.FilterButtonClicked()} disabled={this.FilterButtonVisibility()} > Filter </Button>
                            <Button type="button" color="primary" style={{ marginLeft: "15px" }} onClick={() => this.ClearButtonClicked()} disabled={this.ClearButtonVisibility()}  >Clear</Button>
                        </div>
                    </Form>
                </div>

                <div className="container-fluid" >

                    <h1> Books Available: </h1>
                    <ReactTable
                        data={this.currentMode()}


                        sortable={false}


                        columns={[
                            {
                                Header: "ID",
                                accessor: "id",


                            },

                            {
                                Header: "Author",
                                accessor: "author",


                            },
                            {
                                Header: "Title",
                                accessor: "title",

                            },
                            {
                                Header: "Year",
                                accessor: "year",


                            }


                        ]}

                        defaultPageSize={20}
                        style={{
                            height: "400px" // This will force the table body to overflow and scroll, since there is not enough room
                        }}
                        className="-striped -highlight"

                        getTrProps={(state, rowInfo) => {
                            if (rowInfo && rowInfo.row) {
                                return {
                                    onClick: (e) => {
                                        this.setState({
                                            selected: rowInfo.index,
                                            currentbook: rowInfo.row,

                                        })
                                        console.log(this.state.selected);
                                        //console.log(this.state.rownumber);
                                    },



                                    style: {
                                        background: rowInfo.index === this.state.selected ? '#00afec' : 'white',
                                        color: rowInfo.index === this.state.selected ? 'white' : 'black'
                                    }
                                }
                            } else {
                                return {}
                            }
                        }}
                    />



                    <Container>
                        <Row>
                            <Col xs={12} sm={12} md={4} lg={4}>
                                <Button style={{ width: "100%", marginTop: "10px" }} color="primary" onClick={this.openModalAdd}  > Add Book </Button>{' '}
                            </Col>
                            <Col xs={12} sm={12} md={4} lg={4}>
                                <Button style={{ width: "100%", marginTop: "10px" }} color="primary" onClick={this.openModalEdit} disabled={this.DeleteEditButtonsVisibility()} > Edit Book </Button>

                            </Col>
                            <Col xs={12} sm={12} md={4} lg={4}>
                                <Button style={{ width: "100%", marginTop: "10px" }} color="primary" onClick={this.DeleteSelectedBook} disabled={this.DeleteEditButtonsVisibility()}  > Delete Book </Button>{' '}
                            </Col>
                        </Row>
                    </Container>





                    <AddBook showModal={this.state.showModalAdd} handleClose={this.closeModalAdd} PassData={this.PassBook} />

                    <EditBook showModal={this.state.showModalEdit} handleClose={this.closeModalEdit} PassData={this.PassBookToEdit} SelectedBook={this.state.currentbook} />

                </div>
            </div>
        );
    }
}
