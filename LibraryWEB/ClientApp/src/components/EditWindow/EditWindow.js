﻿import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Row, Col, Container } from 'reactstrap';
import { Form, FormGroup, Label, Input, FormText } from 'reactstrap';

export default class EditWindow extends Component {
    constructor(props) {
        super(props);

        this.state = {

            //ID : "",
            Author: "",
            Title: "",
            Year: "",
            // ButtonDisabled: true,

            showModal: props.showModal,

        }

      
        
    }

    openModal = () => {
        this.setState({
            showModal: !this.state.showModal,
        })
    }

    /*IDChangeHandler = event => {
        this.setState({
           
            ID: event.target.value,

            // ButtonDisabled : false
        });

        console.log(this.state.ID);
    }*/

    AuthorChangeHandler = event => {
        this.setState({
           
            Author: event.target.value,

            // ButtonDisabled : false
        });



        console.log(this.state.Author);
    }

    TitleChangeHandler = event => {

        this.setState({
           
            Title: event.target.value,
            // ButtonDisabled: false
        });

        console.log(this.state.Title);
    }

    YearChangeHandler = event => {
        this.setState({
           
            Year: event.target.value,
            //  ButtonDisabled: false
        });


    }

    CloseButton = () => {

        this.setState({
           // ID:"",
            Author: "",
            Title: "",
            Year: "",
        });

        this.props.handleClose();

    }


    SaveAndClose = () => {
        console.log("SAVEANDCLOSE WAS CALLED");

        let Book = {};

        console.log(this.props.SelectedBook.id);
        console.log(this.props.SelectedBook.author);
        console.log(this.props.SelectedBook.title);
        console.log(this.props.SelectedBook.year);

        /*if (this.state.ID.trim() == "") {
            Book.id = this.props.SelectedBook.id;
        }
        else {
            Book.id = this.state.ID;
        }
        */

        Book.id = this.props.SelectedBook.id;

        if (this.state.Author.trim() == "")
        {
            Book.author = this.props.SelectedBook.author;
        }
        else
        {
            Book.author = this.state.Author;
        }

        if (this.state.Title.trim() == "")
        {
            Book.title = this.props.SelectedBook.title;
        }
        else
        {
            Book.title = this.state.Title;
            //this.props.SelectedBook.author = this.state.Title;
        }

        if (this.state.Year.trim() == "")
        {
            Book.year = this.props.SelectedBook.year;
        }
        else
        {
            Book.year = this.state.Year;
           // this.props.SelectedBook.author = this.state.Year;
        }
        
      
        console.log(Book);

        this.props.PassData(Book);  //passing the created book back

        this.setState({
            //ID: "",
            Author: "",
            Title: "",
            Year: "",
        });

        this.props.handleClose();
    }

    //this.state.ID.trim() == "" &&
     InputsEmpty() {

            if (this.state.Author.trim() == "" && this.state.Title.trim() == "" && this.state.Year.trim() == "")
                return true;
        else
            return false;
    }


    render() {
        return (
            <Modal isOpen={this.props.showModal} className={this.props.className}>
                <ModalHeader> Modal title</ModalHeader>
                <ModalBody>

                    <Form>
                    

                        <FormGroup>
                            <Label for="exampleAuthor">Author</Label>
                            <Input type="Author"  name="email" id="exampleEmail" placeholder={this.props.SelectedBook.author} onChange={this.AuthorChangeHandler} />
                        </FormGroup>

                        <FormGroup>
                            <Label for="exampleTitle">Title</Label>
                            <Input type="Title"  name="password" id="examplePassword" placeholder={this.props.SelectedBook.title} onChange={this.TitleChangeHandler} />
                        </FormGroup>

                        <FormGroup>
                            <Label for="exampleYear">Year</Label>
                            <Input type="Year" name="password" id="examplePassword" placeholder={this.props.SelectedBook.year} onChange={this.YearChangeHandler} />
                        </FormGroup>


                    </Form>

                </ModalBody>

                <ModalFooter>
                    <Button color="primary" disabled={this.InputsEmpty()} onClick={() => this.SaveAndClose()} >Apply CHanges </Button>

                    <Button color="secondary" onClick={() => this.CloseButton()} >Cancel</Button>

                </ModalFooter>
            </Modal>
        );
    }
}
